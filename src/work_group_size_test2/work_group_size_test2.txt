select device Devastator 
================================
kernel(COPY): 16777216 byte
localWorkSize: 1x1 
         __________s__m__u__n
  queued:        123212854240 ns.
  sumbit:        123212874301 ns.
 que-sum:               20061 ns.
   start:        123574327301 ns.
 sum-sta:           361453000 ns.
    quit:        123934762161 ns.
 sta-qui:           360434860 ns.
   total:           721907921 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 1x2 
         __________s__m__u__n
  queued:        123975294661 ns.
  sumbit:        123975323529 ns.
 que-sum:               28868 ns.
   start:        124156682260 ns.
 sum-sta:           181358731 ns.
    quit:        124337582290 ns.
 sta-qui:           180900030 ns.
   total:           362287629 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 1x4 
         __________s__m__u__n
  queued:        124375230383 ns.
  sumbit:        124375265598 ns.
 que-sum:               35215 ns.
   start:        124468456259 ns.
 sum-sta:            93190661 ns.
    quit:        124561321659 ns.
 sta-qui:            92865400 ns.
   total:           186091276 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 1x8 
         __________s__m__u__n
  queued:        124598949566 ns.
  sumbit:        124598988477 ns.
 que-sum:               38911 ns.
   start:        124653553914 ns.
 sum-sta:            54565437 ns.
    quit:        124707831104 ns.
 sta-qui:            54277190 ns.
   total:           108881538 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 1x16 
         __________s__m__u__n
  queued:        124749049309 ns.
  sumbit:        124749079792 ns.
 que-sum:               30483 ns.
   start:        124791081831 ns.
 sum-sta:            42002039 ns.
    quit:        124832810601 ns.
 sta-qui:            41728770 ns.
   total:            83761292 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 1x32 
         __________s__m__u__n
  queued:        124861425720 ns.
  sumbit:        124861470330 ns.
 que-sum:               44610 ns.
   start:        124913770989 ns.
 sum-sta:            52300659 ns.
    quit:        124965940599 ns.
 sta-qui:            52169610 ns.
   total:           104514879 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 1x64 
         __________s__m__u__n
  queued:        125004870060 ns.
  sumbit:        125004890867 ns.
 que-sum:               20807 ns.
   start:        125065533557 ns.
 sum-sta:            60642690 ns.
    quit:        125126036727 ns.
 sta-qui:            60503170 ns.
   total:           121166667 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 1x128 
         __________s__m__u__n
  queued:        125158312026 ns.
  sumbit:        125158324278 ns.
 que-sum:               12252 ns.
   start:        125280732396 ns.
 sum-sta:           122408118 ns.
    quit:        125402901646 ns.
 sta-qui:           122169250 ns.
   total:           244589620 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 2x1 
         __________s__m__u__n
  queued:        125439048371 ns.
  sumbit:        125439087827 ns.
 que-sum:               39456 ns.
   start:        125623165422 ns.
 sum-sta:           184077595 ns.
    quit:        125806870692 ns.
 sta-qui:           183705270 ns.
   total:           367822321 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 2x2 
         __________s__m__u__n
  queued:        125846086473 ns.
  sumbit:        125846109383 ns.
 que-sum:               22910 ns.
   start:        125940387317 ns.
 sum-sta:            94277934 ns.
    quit:        126034326037 ns.
 sta-qui:            93938720 ns.
   total:           188239564 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 2x4 
         __________s__m__u__n
  queued:        126074489995 ns.
  sumbit:        126074523305 ns.
 que-sum:               33310 ns.
   start:        126121420669 ns.
 sum-sta:            46897364 ns.
    quit:        126168133889 ns.
 sta-qui:            46713220 ns.
   total:            93643894 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 2x8 
         __________s__m__u__n
  queued:        126206197817 ns.
  sumbit:        126206233276 ns.
 que-sum:               35459 ns.
   start:        126237296486 ns.
 sum-sta:            31063210 ns.
    quit:        126268236536 ns.
 sta-qui:            30940050 ns.
   total:            62038719 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 2x16 
         __________s__m__u__n
  queued:        126306513074 ns.
  sumbit:        126306539649 ns.
 que-sum:               26575 ns.
   start:        126332413393 ns.
 sum-sta:            25873744 ns.
    quit:        126358127503 ns.
 sta-qui:            25714110 ns.
   total:            51614429 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 2x32 
         __________s__m__u__n
  queued:        126397219947 ns.
  sumbit:        126397234914 ns.
 que-sum:               14967 ns.
   start:        126433844783 ns.
 sum-sta:            36609869 ns.
    quit:        126470339043 ns.
 sta-qui:            36494260 ns.
   total:            73119096 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 2x64 
         __________s__m__u__n
  queued:        126503427008 ns.
  sumbit:        126503445143 ns.
 que-sum:               18135 ns.
   start:        126546952286 ns.
 sum-sta:            43507143 ns.
    quit:        126590144846 ns.
 sta-qui:            43192560 ns.
   total:            86717838 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 4x1 
         __________s__m__u__n
  queued:        126630488484 ns.
  sumbit:        126630502772 ns.
 que-sum:               14288 ns.
   start:        126724220077 ns.
 sum-sta:            93717305 ns.
    quit:        126817620427 ns.
 sta-qui:            93400350 ns.
   total:           187131943 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 4x2 
         __________s__m__u__n
  queued:        126857338674 ns.
  sumbit:        126857366235 ns.
 que-sum:               27561 ns.
   start:        126903614112 ns.
 sum-sta:            46247877 ns.
    quit:        126949621522 ns.
 sta-qui:            46007410 ns.
   total:            92282848 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 4x4 
         __________s__m__u__n
  queued:        126988884597 ns.
  sumbit:        126988898725 ns.
 que-sum:               14128 ns.
   start:        127014042144 ns.
 sum-sta:            25143419 ns.
    quit:        127039018974 ns.
 sta-qui:            24976830 ns.
   total:            50134377 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 4x8 
         __________s__m__u__n
  queued:        127078212792 ns.
  sumbit:        127078224149 ns.
 que-sum:               11357 ns.
   start:        127092826324 ns.
 sum-sta:            14602175 ns.
    quit:        127107281364 ns.
 sta-qui:            14455040 ns.
   total:            29068572 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 4x16 
         __________s__m__u__n
  queued:        127147123662 ns.
  sumbit:        127147163916 ns.
 que-sum:               40254 ns.
   start:        127162412875 ns.
 sum-sta:            15248959 ns.
    quit:        127177488855 ns.
 sta-qui:            15075980 ns.
   total:            30365193 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 4x32 
         __________s__m__u__n
  queued:        127214084545 ns.
  sumbit:        127214091521 ns.
 que-sum:                6976 ns.
   start:        127232426775 ns.
 sum-sta:            18335254 ns.
    quit:        127250651185 ns.
 sta-qui:            18224410 ns.
   total:            36566640 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 8x1 
         __________s__m__u__n
  queued:        127290349685 ns.
  sumbit:        127290386773 ns.
 que-sum:               37088 ns.
   start:        127338689379 ns.
 sum-sta:            48302606 ns.
    quit:        127386693009 ns.
 sta-qui:            48003630 ns.
   total:            96343324 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 8x2 
         __________s__m__u__n
  queued:        127428864191 ns.
  sumbit:        127428892581 ns.
 que-sum:               28390 ns.
   start:        127454031789 ns.
 sum-sta:            25139208 ns.
    quit:        127478918919 ns.
 sta-qui:            24887130 ns.
   total:            50054728 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 8x4 
         __________s__m__u__n
  queued:        127520991478 ns.
  sumbit:        127521011671 ns.
 que-sum:               20193 ns.
   start:        127532492880 ns.
 sum-sta:            11481209 ns.
    quit:        127543799790 ns.
 sta-qui:            11306910 ns.
   total:            22808312 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 8x8 
         __________s__m__u__n
  queued:        127583447923 ns.
  sumbit:        127583468691 ns.
 que-sum:               20768 ns.
   start:        127595778579 ns.
 sum-sta:            12309888 ns.
    quit:        127607936029 ns.
 sta-qui:            12157450 ns.
   total:            24488106 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 8x16 
         __________s__m__u__n
  queued:        127647884767 ns.
  sumbit:        127647900141 ns.
 que-sum:               15374 ns.
   start:        127661377731 ns.
 sum-sta:            13477590 ns.
    quit:        127674547931 ns.
 sta-qui:            13170200 ns.
   total:            26663164 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 16x1 
         __________s__m__u__n
  queued:        127711459286 ns.
  sumbit:        127711473554 ns.
 que-sum:               14268 ns.
   start:        127735887898 ns.
 sum-sta:            24414344 ns.
    quit:        127760077448 ns.
 sta-qui:            24189550 ns.
   total:            48618162 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 16x2 
         __________s__m__u__n
  queued:        127799327444 ns.
  sumbit:        127799345337 ns.
 que-sum:               17893 ns.
   start:        127809919908 ns.
 sum-sta:            10574571 ns.
    quit:        127820353788 ns.
 sta-qui:            10433880 ns.
   total:            21026344 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 16x4 
         __________s__m__u__n
  queued:        127860160536 ns.
  sumbit:        127860181559 ns.
 que-sum:               21023 ns.
   start:        127869900157 ns.
 sum-sta:             9718598 ns.
    quit:        127879472637 ns.
 sta-qui:             9572480 ns.
   total:            19312101 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 16x8 
         __________s__m__u__n
  queued:        127909136867 ns.
  sumbit:        127909158669 ns.
 que-sum:               21802 ns.
   start:        127921430911 ns.
 sum-sta:            12272242 ns.
    quit:        127933558241 ns.
 sta-qui:            12127330 ns.
   total:            24421374 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 32x1 
         __________s__m__u__n
  queued:        127966160264 ns.
  sumbit:        127966175096 ns.
 que-sum:               14832 ns.
   start:        127976182254 ns.
 sum-sta:            10007158 ns.
    quit:        127986043754 ns.
 sta-qui:             9861500 ns.
   total:            19883490 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 32x2 
         __________s__m__u__n
  queued:        128025756425 ns.
  sumbit:        128025794576 ns.
 que-sum:               38151 ns.
   start:        128035352528 ns.
 sum-sta:             9557952 ns.
    quit:        128044759768 ns.
 sta-qui:             9407240 ns.
   total:            19003343 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 32x4 
         __________s__m__u__n
  queued:        128077049167 ns.
  sumbit:        128077066020 ns.
 que-sum:               16853 ns.
   start:        128087610023 ns.
 sum-sta:            10544003 ns.
    quit:        128097997533 ns.
 sta-qui:            10387510 ns.
   total:            20948366 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 64x1 
         __________s__m__u__n
  queued:        128136595612 ns.
  sumbit:        128136629580 ns.
 que-sum:               33968 ns.
   start:        128147798354 ns.
 sum-sta:            11168774 ns.
    quit:        128158853734 ns.
 sta-qui:            11055380 ns.
   total:            22258122 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 64x2 
         __________s__m__u__n
  queued:        128197254792 ns.
  sumbit:        128197260796 ns.
 que-sum:                6004 ns.
   start:        128209138503 ns.
 sum-sta:            11877707 ns.
    quit:        128220922903 ns.
 sta-qui:            11784400 ns.
   total:            23668111 ns.
================================
kernel(COPY): 16777216 byte
localWorkSize: 128x1 
         __________s__m__u__n
  queued:        128259074315 ns.
  sumbit:        128259080964 ns.
 que-sum:                6649 ns.
   start:        128270821372 ns.
 sum-sta:            11740408 ns.
    quit:        128282452482 ns.
 sta-qui:            11631110 ns.
   total:            23378167 ns.
 721907921	 362287629	 186091276	 108881538	  83761292	 104514879	 121166667	 244589620	
 367822321	 188239564	  93643894	  62038719	  51614429	  73119096	  86717838	         0	
 187131943	  92282848	  50134377	  29068572	  30365193	  36566640	         0	         0	
  96343324	  50054728	  22808312	  24488106	  26663164	         0	         0	         0	
  48618162	  21026344	  19312101	  24421374	         0	         0	         0	         0	
  19883490	  19003343	  20948366	         0	         0	         0	         0	         0	
  22258122	  23668111	         0	         0	         0	         0	         0	         0	
  23378167	         0	         0	         0	         0	         0	         0	         0	
