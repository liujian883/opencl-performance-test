Number of platforms: 1
-Platforms 0:
  CL_PLATFORM_NAME      : AMD Accelerated Parallel Processing
  CL_PLATFORM_VENDOR    : Advanced Micro Devices, Inc.
  CL_PLATFORM_VERSION   : OpenCL 1.2 AMD-APP (1113.2)
  CL_PLATFORM_PROFILE   : FULL_PROFILE
  CL_PLATFORM_EXTENSIONS: cl_khr_icd cl_amd_event_callback cl_amd_offline_devices
  Number of devices     : 2
---Devices 0:
    CL_DEVICE_TYPE                         : CL_DEVICE_TYPE_GPU
    CL_DEVICE_VENDOR_ID                    : 4098
    CL_DEVICE_MAX_COMPUTE_UNITS            : 6
    CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS     : 3
    CL_DEVICE_MAX_WORK_GROUP_SIZE          : 256
    CL_DEVICE_MAX_WORK_ITEM_SIZES          : (256, 256, 256)
    CL_DEVICE_MAX_CLOCK_FREQUENCY          : 800 MHz
    CL_DEVICE_ADDRESS_BITS                 : 32 bits
    CL_DEVICE_MAX_MEM_ALLOC_SIZE           : 150208512 bytes
    CL_DEVICE_NAME                         : Devastator

---Devices 1:
    CL_DEVICE_TYPE                         : CL_DEVICE_TYPE_CPU
    CL_DEVICE_VENDOR_ID                    : 4098
    CL_DEVICE_MAX_COMPUTE_UNITS            : 4
    CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS     : 3
    CL_DEVICE_MAX_WORK_GROUP_SIZE          : 1024
    CL_DEVICE_MAX_WORK_ITEM_SIZES          : (1024, 1024, 1024)
    CL_DEVICE_MAX_CLOCK_FREQUENCY          : 1400 MHz
    CL_DEVICE_ADDRESS_BITS                 : 64 bits
    CL_DEVICE_MAX_MEM_ALLOC_SIZE           : 2147483648 bytes
    CL_DEVICE_NAME                         : AMD A10-5800K APU with Radeon(tm) HD Graphics  


