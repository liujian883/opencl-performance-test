__kernel void work_group_size_test_kernel_1(__global char *a) {
	int x = get_global_id(0);
	int i;
	char var1;

	for (i = 0; i < 256; i++) {
		var1 = a[i*256 + x];

		a[i*256 + x] = var1 + var1;
	}
}

__kernel void work_group_size_test_kernel_2(__global char *a, __global char *b) {
	int x = get_global_id(0);
	int i;
	char var1;
	char var2;

	for (i = 0; i < 256; i++) {
		var1 = a[i*256 + x];
		var2 = b[i*256 + x];

		a[i*256 + x] = var1 + var2;
	}
}

__kernel void work_group_size_test_kernel_3(__global char *a, __global char *b, __global char *c) {
	int x = get_global_id(0);
	int i;
	char var1;
	char var2;
	char var3;

	for (i = 0; i < 256; i++) {
		var1 = a[i*256 + x];
		var2 = b[i*256 + x];
		var3 = b[i*256 + x];

		a[i*256 + x] = var1 + var2 + var3;
	}
}

__kernel void work_group_size_test_kernel_4(__global char *a, __global char *b, __global char *c, __global char *d) {
	int x = get_global_id(0);
	int i;
	char var1;
	char var2;
	char var3;
	char var4;
	for (i = 0; i < 256; i++) {
		var1 = a[i*256 + x];
		var2 = b[i*256 + x];
		var3 = b[i*256 + x];
		var4 = d[i*256 + x];
		a[i*256 + x] = var1 + var2 + var3 + var4;
	}
}
