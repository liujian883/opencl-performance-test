#include <stdio.h>
#include <stdlib.h>
#include <alloca.h>

#include <stdbool.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <math.h>

#include <time.h>
#include <sys/time.h>

#include <CL/cl.h>

#define DEVICE_TYPE CL_DEVICE_TYPE_GPU

const unsigned int ARRAY_SIZE_1 = 2048;
const unsigned int ARRAY_SIZE_2 = 40000;
const unsigned int ARRAY_SIZE_3 = 81920000;

// Function to check and handle OpenCL errors
inline void checkErr(cl_int ret_num, cl_int pas_num, const char * name){
    if (ret_num != pas_num) {
        fprintf(stderr, "ERROR: %s (%d)\n", name, ret_num);
//        releaseAll(context, command_queue, program, kernel, mem_objects);
        exit(EXIT_FAILURE);
    }
}

void releaseAll(cl_context context, cl_command_queue command_queue,
             cl_program program, cl_kernel kernel, cl_mem mem_objects[3]){
	cl_int ret_num;
	cl_int i;
    for (i = 0; i < 3; i++){
        if (mem_objects[i] != 0){
            ret_num = clReleaseMemObject(mem_objects[i]);
            checkErr(ret_num, CL_SUCCESS, "clReleaseMemObject");
		}
    }
    if (command_queue != 0){
        ret_num = clReleaseCommandQueue(command_queue);
        checkErr(ret_num, CL_SUCCESS, "clReleaseCommandQueue");
	}

    if (kernel != 0){
        ret_num = clReleaseKernel(kernel);
        checkErr(ret_num, CL_SUCCESS, "clReleaseKernel");
	}

    if (program != 0){
        ret_num = clReleaseProgram(program);
        checkErr(ret_num, CL_SUCCESS, "clReleaseProgram");
	}

    if (context != 0){
        ret_num = clReleaseContext(context);
        checkErr(ret_num, CL_SUCCESS, "clReleaseContext");
	}
}

void CL_CALLBACK contextCallback(
					const char * err_info, 
					const void * private_info, 
					size_t cb, 
					void * user_data){
	fprintf(stderr, "Error occured during context use: %s\n", err_info);
//	releaseAll(context, command_queue, program, kernel, mem_objects);
	exit(EXIT_FAILURE);
}

cl_context createContext(){
    cl_int ret_num;
    cl_uint num_platforms;
	cl_uint num_devices;
    cl_platform_id * platform_ids;
	cl_device_id * device_ids;
    cl_context context = NULL;
	cl_uint i;

	// Select an OpenCL platform to run on.  
	ret_num = clGetPlatformIDs(0, NULL, &num_platforms);
	checkErr( 
		(ret_num != CL_SUCCESS) ? ret_num : (num_platforms <= 0 ? -1 : CL_SUCCESS), 
		CL_SUCCESS, "clGetPlatformIDs"); 

	platform_ids = (cl_platform_id *)alloca(sizeof(cl_platform_id) * num_platforms);

    ret_num = clGetPlatformIDs(num_platforms, platform_ids, NULL);
    checkErr(ret_num, CL_SUCCESS, "clGetPlatformIDs");

	// Iterate through the list of platforms until we find one that supports
	// a GPU device, otherwise fail with an error.
	device_ids = NULL;
	for (i = 0; i < num_platforms; i++){
		ret_num = clGetDeviceIDs(
					platform_ids[i], 
					DEVICE_TYPE, 
					0,
					NULL,
					&num_devices);
		checkErr( 
			(ret_num != CL_SUCCESS) ? ret_num : (num_devices <= 0 ? -1 : CL_SUCCESS), 
			CL_SUCCESS, "clGetDeviceIDs"); 
		device_ids = (cl_device_id *)alloca(sizeof(cl_device_id) * num_devices);
		ret_num = clGetDeviceIDs(
					platform_ids[i],
					DEVICE_TYPE,
					num_devices, 
					device_ids, 
					NULL);
		checkErr(ret_num, CL_SUCCESS, "clGetDeviceIDs");
		break;
	}
	// Create an OpenCL context on the selected platform.  
    cl_context_properties contextProperties[] ={
								CL_CONTEXT_PLATFORM,
								(cl_context_properties)platform_ids[i],
								0};
    context = clCreateContext(
				contextProperties, 
				num_devices,
				device_ids, 
				&contextCallback,
				NULL, 
				&ret_num);
	checkErr(ret_num, CL_SUCCESS, "clCreateContext");

    return context;
}

cl_command_queue createCommandQueue(cl_context context, cl_device_id *device){
    cl_int ret_num;
    cl_uint num_devices;
    cl_device_id *device_ids;
    cl_command_queue command_queue = NULL;

    // First get the size of the devices buffer
    ret_num = clGetContextInfo(context, CL_CONTEXT_NUM_DEVICES, sizeof(cl_uint), &num_devices, NULL);
    checkErr(ret_num, CL_SUCCESS, "clGetContextInfo");

    if (num_devices <= 0)
    {
        fprintf(stderr, "No devices available.\n");
        return NULL;
    }

    // Allocate memory for the devices buffer
    device_ids = (cl_device_id *)alloca(sizeof(cl_device_id) * num_devices);
    ret_num = clGetContextInfo(context, CL_CONTEXT_DEVICES, sizeof(cl_device_id) * num_devices, device_ids, NULL);
    checkErr(ret_num, CL_SUCCESS, "clGetContextInfo");

    // Choose the first available device. 
    command_queue = clCreateCommandQueue(context, device_ids[0], CL_QUEUE_PROFILING_ENABLE, &ret_num);
    checkErr(ret_num, CL_SUCCESS, "clCreateCommandQueue");

    *device = device_ids[0];
    return command_queue;
}

cl_program createProgram(cl_context context, cl_device_id device, const char* file_name)
{
    cl_int ret_num;
    cl_program program;

	struct stat statbuf;

	FILE *fh = fopen(file_name, "r");
	if (fh == 0) {
	fprintf(stderr, "Couldn't open %s\n", file_name);
	return NULL;
	}

	stat(file_name, &statbuf);
	char *source = (char *) alloca(statbuf.st_size + 1);
	if (source == NULL) {
	fprintf(stderr, "malloc failed\n");
	return NULL;
	}

	ret_num = fread(source, statbuf.st_size, 1, fh);
	checkErr(ret_num, 1, "fread");

	source[statbuf.st_size] = '\0';


    program = clCreateProgramWithSource(context, 1,
                                        (const char**)&source,
                                        NULL, NULL);
    if (program == NULL)
    {
		fprintf(stderr, "Failed to create CL program from source.\n");
        return NULL;
    }

    ret_num = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (ret_num != CL_SUCCESS)
    {
        // Determine the reason for the error
        char buildLog[16384];
        clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG,
                              sizeof(buildLog), buildLog, NULL);

        fprintf(stderr, "Error in kernel:\n");
		fprintf(stderr, "%s\n", buildLog);
        clReleaseProgram(program);
        return NULL;
    }

    return program;
}

bool createMemObjects(cl_context context, cl_mem mem_objects[3],
                      cl_float2 *a, cl_float2 *b)
{
    mem_objects[0] = clCreateBuffer(context, CL_MEM_READ_ONLY,
                                   sizeof(cl_float2) * ARRAY_SIZE_1, NULL, NULL);
    mem_objects[1] = clCreateBuffer(context, CL_MEM_READ_ONLY,
                                   sizeof(cl_float2) * ARRAY_SIZE_2, NULL, NULL);
    mem_objects[2] = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                   sizeof(cl_float) * ARRAY_SIZE_3, NULL, NULL);

    if (mem_objects[0] == NULL || mem_objects[1] == NULL || mem_objects[2] == NULL)
    {
        fprintf(stderr, "Error creating memory objects.\n");
        return false;
    }

    return true;
}

cl_float distance(cl_float2 a, cl_float2 b){
	return sqrtf(powf((a.x - b.x), 2) + powf((a.y - b.y), 2));
}

int main(int argc, char** argv)
{
    cl_context context = 0;
    cl_command_queue command_queue = 0;
    cl_program program = 0;
    cl_device_id device = 0;
    cl_kernel kernel = 0;
    cl_mem mem_objects[3] = { 0, 0, 0 };
	
	cl_float2 *a;
	cl_float2 *b;
	cl_float *result, tmp;
    cl_int ret_num;
    cl_uint i, j;

	cl_event write_event_1;
	cl_event write_event_2;
	cl_event kernel_event;	
	cl_event read_event;
	
	cl_ulong queued, sumbit, start, quit, use;

	struct timeval begin, end;
	//~ double use;

    a = (cl_float2 *) malloc(sizeof(cl_float2) * ARRAY_SIZE_1);
    b = (cl_float2 *) malloc(sizeof(cl_float2) * ARRAY_SIZE_2);
    result = (cl_float *) malloc(sizeof(cl_float) * ARRAY_SIZE_3);

	srand((int)time(0));
	
    for (i = 0; i < ARRAY_SIZE_1; i++)
    {
        a[i].x = (cl_float)(64.0*rand()/(RAND_MAX+1.0));
        a[i].y = (cl_float)(64.0*rand()/(RAND_MAX+1.0));
    }
    
    for (i = 0; i < ARRAY_SIZE_2; i++)
    {
        b[i].x = (cl_float)(64.0*rand()/(RAND_MAX+1.0));
        b[i].y = (cl_float)(64.0*rand()/(RAND_MAX+1.0));
    }




    // Create an OpenCL context on first available platform
    context = createContext();
    if (context == NULL)
    {
        fprintf(stderr, "Failed to create OpenCL context.\n");
        return 1;
    }

    // Create a command-queue on the first device available
    // on the created context
    command_queue = createCommandQueue(context, &device);
    if (command_queue == NULL)
    {
        releaseAll(context, command_queue, program, kernel, mem_objects);
        return 1;
    }

    // Create OpenCL program from HelloWorld.cl kernel source
    program = createProgram(context, device, "distance.cl");
    if (program == NULL)
    {
        releaseAll(context, command_queue, program, kernel, mem_objects);
        return 1;
    }

    // Create OpenCL kernel
    kernel = clCreateKernel(program, "distance_kernel", NULL);
    if (kernel == NULL)
    {
        fprintf(stderr, "Failed to create kernel.\n");
        releaseAll(context, command_queue, program, kernel, mem_objects);
        return 1;
    }

    // Create memory objects that will be used as arguments to
    // kernel.  First create host memory arrays that will be
    // used to store the arguments to the kernel


    if (!createMemObjects(context, mem_objects, a, b))
    {
        releaseAll(context, command_queue, program, kernel, mem_objects);
        return 1;
    }
    
    //writebuffer
    ret_num = clEnqueueWriteBuffer(command_queue, mem_objects[0], CL_TRUE,
                                 0, ARRAY_SIZE_1 * sizeof(cl_float2), a,
                                 0, NULL, &write_event_1);
	checkErr(ret_num, CL_SUCCESS, "clEnqueueWriteBuffer");
	
	ret_num = clEnqueueWriteBuffer(command_queue, mem_objects[1], CL_TRUE,
                                 0, ARRAY_SIZE_2 * sizeof(cl_float2), b,
                                 0, NULL, &write_event_2);
	while(clWaitForEvents(1, &write_event_2) != CL_SUCCESS){
		
	}
	checkErr(ret_num, CL_SUCCESS, "clEnqueueWriteBuffer");
    

    // Set the kernel arguments (result, a, b)
    ret_num = clSetKernelArg(kernel, 0, sizeof(cl_mem), &mem_objects[0]);
    ret_num |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &mem_objects[1]);
    ret_num |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &mem_objects[2]);
	checkErr(ret_num, CL_SUCCESS, "clSetKernelArg");

    //~ size_t globalWorkSize[2] = { ARRAY_SIZE_1, ARRAY_SIZE_2 / 8 };
    size_t globalWorkSize[2] = { ARRAY_SIZE_1, ARRAY_SIZE_2 };
    size_t localWorkSize[2] = { 32, 2 };

    // Queue the kernel up for execution across the array
    ret_num = clEnqueueNDRangeKernel(command_queue, kernel, 2, NULL,
                                    globalWorkSize, localWorkSize,
                                    0, NULL, &kernel_event);
	checkErr(ret_num, CL_SUCCESS, "clEnqueueNDRangeKernel");

    // Read the output buffer back to the Host
    ret_num = clEnqueueReadBuffer(command_queue, mem_objects[2], CL_TRUE,
                                 0, ARRAY_SIZE_3 * sizeof(float), result,
                                 0, NULL, &read_event);
	checkErr(ret_num, CL_SUCCESS, "clEnqueueReadBuffer");

    // Output the result buffer
    //~ for (i = 0; i < ARRAY_SIZE_1; i++)
    //~ {
        //~ printf("%2.5f ", result[i]);
    //~ }
	
	ret_num = clGetEventProfilingInfo(write_event_1, CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong), &queued, NULL);
	ret_num |= clGetEventProfilingInfo(write_event_1, CL_PROFILING_COMMAND_SUBMIT, sizeof(cl_ulong), &sumbit, NULL);
	ret_num |= clGetEventProfilingInfo(write_event_1, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
	ret_num |= clGetEventProfilingInfo(write_event_1, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &quit, NULL);
	checkErr(ret_num, CL_SUCCESS, "clGetEventProfilingInfo");
	printf("clEnqueueWriteBuffer(cl_float2 a[2048])\n");
	printf("         __________s__m__u__n\n");
	printf("  queued:%20.0ld ns.\n", queued);
	printf("  sumbit:%20.0ld ns.\n", sumbit);
	printf(" que-sum:%20.0ld ns.\n", sumbit - queued);
	printf("   start:%20.0ld ns.\n", start);
	printf(" sum-sta:%20.0ld ns.\n", start - sumbit);
	printf("    quit:%20.0ld ns.\n", quit);
	printf(" sta-qui:%20.0ld ns.\n", quit - start);
	printf("   total:%20.0ld ns.\n", quit - queued);

	ret_num = clGetEventProfilingInfo(write_event_2, CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong), &queued, NULL);
	ret_num |= clGetEventProfilingInfo(write_event_2, CL_PROFILING_COMMAND_SUBMIT, sizeof(cl_ulong), &sumbit, NULL);
	ret_num |= clGetEventProfilingInfo(write_event_2, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
	ret_num |= clGetEventProfilingInfo(write_event_2, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &quit, NULL);
	checkErr(ret_num, CL_SUCCESS, "clGetEventProfilingInfo");
	printf("clEnqueueWriteBuffer(cl_float2 b[40000])\n");
	printf("         __________s__m__u__n\n");
	printf("  queued:%20.0ld ns.\n", queued);
	printf("  sumbit:%20.0ld ns.\n", sumbit);
	printf(" que-sum:%20.0ld ns.\n", sumbit - queued);
	printf("   start:%20.0ld ns.\n", start);
	printf(" sum-sta:%20.0ld ns.\n", start - sumbit);
	printf("    quit:%20.0ld ns.\n", quit);
	printf(" sta-qui:%20.0ld ns.\n", quit - start);
	printf("   total:%20.0ld ns.\n", quit - queued);

	ret_num = clGetEventProfilingInfo(kernel_event, CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong), &queued, NULL);
	ret_num |= clGetEventProfilingInfo(kernel_event, CL_PROFILING_COMMAND_SUBMIT, sizeof(cl_ulong), &sumbit, NULL);
	ret_num |= clGetEventProfilingInfo(kernel_event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
	ret_num |= clGetEventProfilingInfo(kernel_event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &quit, NULL);
	checkErr(ret_num, CL_SUCCESS, "clGetEventProfilingInfo");
	printf("clEnqueueNDRangeKernel\n");
	printf("         __________s__m__u__n\n");
	printf("  queued:%20.0ld ns.\n", queued);
	printf("  sumbit:%20.0ld ns.\n", sumbit);
	printf(" que-sum:%20.0ld ns.\n", sumbit - queued);
	printf("   start:%20.0ld ns.\n", start);
	printf(" sum-sta:%20.0ld ns.\n", start - sumbit);
	printf("    quit:%20.0ld ns.\n", quit);
	printf(" sta-qui:%20.0ld ns.\n", quit - start);
	printf("   total:%20.0ld ns.\n", quit - queued);

	ret_num = clGetEventProfilingInfo(read_event, CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong), &queued, NULL);
	ret_num |= clGetEventProfilingInfo(read_event, CL_PROFILING_COMMAND_SUBMIT, sizeof(cl_ulong), &sumbit, NULL);
	ret_num |= clGetEventProfilingInfo(read_event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
	ret_num |= clGetEventProfilingInfo(read_event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &quit, NULL);
	checkErr(ret_num, CL_SUCCESS, "clGetEventProfilingInfo");
	printf("clEnqueueReadBuffer(cl_float result[81920000])\n");
	printf("         __________s__m__u__n\n");
	printf("  queued:%20.0ld ns.\n", queued);
	printf("  sumbit:%20.0ld ns.\n", sumbit);
	printf(" que-sum:%20.0ld ns.\n", sumbit - queued);
	printf("   start:%20.0ld ns.\n", start);
	printf(" sum-sta:%20.0ld ns.\n", start - sumbit);
	printf("    quit:%20.0ld ns.\n", quit);
	printf(" sta-qui:%20.0ld ns.\n", quit - start);
	printf("   total:%20.0ld ns.\n", quit - queued);


	gettimeofday(&begin, NULL);	

	for (i = 0; i < ARRAY_SIZE_2; i++){
		for (j = 0; j < ARRAY_SIZE_1; j++){
			//~ tmp = distance(a[j], b[i]);
			//~ if (abs(tmp - result[(i << 11) + j]) <= 0.001){
			//~ result[i] = tmp;
			//~ }else{
				//~ printf("error. %f %f\n", tmp, result[(i << 11) + j]);
				//~ exit(EXIT_FAILURE);
			//~ }
			result[(i << 11) + j] = distance(a[j], b[i]);
			
		}
		//~ printf("%f %f %f|%f %f %f \n", a[i].x, a[i].y, a[i].z, b[0].x, b[0].y, b[0].z);
		//~ printf("%d %f\n", i, result[i*ARRAY_SIZE_2]);
	}
	
	gettimeofday(&end, NULL);


	use = 1000000 * (end.tv_sec - begin.tv_sec) + end.tv_usec - begin.tv_usec;
	use *= 1000;
	printf("         __________s__m__u__n\n");
	printf("CPU Time:%20.0ld ns.(1 core)\n", use);


    //printf("Executed program succesfully.\n");
    releaseAll(context, command_queue, program, kernel, mem_objects);

	free((void*)a);
	free((void*)b);
	free((void*)result);

    return 0;
}
