__kernel void distance_kernel(__global const float2 *a,
						__global const float2 *b,
						__global float *result)
{
    int x = get_global_id(0);
    int y = get_global_id(1);

    result[(y << 11) + x] = distance(a[x], b[y]);
    //~ y += 10000;
    //~ result[(y << 11) + x] = distance(a[x], b[y]);
    //~ y += 10000;
    //~ result[(y << 11) + x] = distance(a[x], b[y]);
    //~ y += 10000;
    //~ result[(y << 11) + x] = distance(a[x], b[y]);
}
